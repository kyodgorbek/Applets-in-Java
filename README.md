# Applets-in-Java

import java.awt.*;
import java.awt.event*;
import java.awt.image.*;
import java.net.*;
import java.io.*;

public class ImageViewee extends Frame
  implements ActionListener
  {  public ImageViewer()
     {
     { setTitle("ImageViewer");
     
     MenuBar mBar = new MenuBar();
     Menu m = new Menu("File");
     MenuItem m1 = new MenuItem("Open");
     m1.addActionListener(this);
     m.add(m1);
     MenuItem m2 = new MenuItem("Exit");
     m2.addActionListener(this);
     m.add(m2);
     mbar.add(m);
     addWindowListener(new WindowAdapter()
     {  public void windowClosing(WindowEvent e)
        { System.exit(0);}
     } );
     setMenuBar(mbar);
     setSize(300, 400);
     show();
   }
   
   public void actionPerformed(ActionEvent evt)
   {  
      String arg = evtgetActionCommand();
      if (arg.equals("Open"))
      { FileDialog d = new FileDialog(this,
               "Open image file", FileDialog.LOAD);
               d.setFile("*.gif");
               d.setDirectory(lastDir);
               d.show();
               String f = d.getFile();
               lastDir = d.getDirectory();
               if (f != null)
                  image = Toolkit.getDefaultToolkit().getImage(lastDir
                + f);
                     repaint();
                    }
                    else if(arg.equals("Exit")) System.exit(0);
                }
                
                public void paint(Graphics g)
                {  g.translate(getInsets().left, getInsets().top);
                   if (image != null)
                      g.drawImage(image, 0, 0, this);
                      
                }
                
                public static void main(String args[])
                {
                    new ImageViewer();
                }   
                
                private Image image = null;
                private String lastDir = "";
    
           }            
      
  
